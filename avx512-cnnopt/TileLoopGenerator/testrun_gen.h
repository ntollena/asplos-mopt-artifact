#pragma once
#include "ukr.h"
#include "omp.h"
#include "transpose_avx512.h"
#include "transpose.h"
#include "ukr10x2vCnnb1f256x68y68c128r3s3.h"
#include "ukr10x2vGemmb1f256x68y68c128r3s3AS.h"
#include "ukr4x2vGemmb1f256x68y68c128r3s3.h"
void testrun(float* A ,float*B, float*C, float*oriB ){
#pragma omp parallel num_threads(8)
{
int tid = omp_get_thread_num();
    int Nx = 68;
    int Ny = 68;
    int Nh = 3;
    int  Astrides[16] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    int b1 = 0;
for (int fpck = (tid%8)*16; fpck < uNf; fpck+=8*16){
for(int cwh = (tid/8)*16; cwh < uNc*uNw*uNh/16*16; cwh+=16*1){
transpose16x16_avx512(oriB+ (fpck+0)*uNc*uNw*uNh + cwh, B + fpck*uNc*uNw*uNh + cwh* 16 + 0, uNc*uNw*uNh, 16);
}
}
#pragma omp barrier// begin push button generated block
for(int c5=0;c5<128+0;c5+=128)
{
for(int xy5=0;xy5<4624+0;xy5+=4624)
{
for(int f5=0;f5<256+0;f5+=256)
{
for(int c4=c5;c4<min(128, 128+c5);c4+=128)
{
for(int f4=f5;f4<min(256, 256+f5);f4+=224)
{
for(int xy4=xy5;xy4<min(4624, 4624+xy5);xy4+=4624)
{
for(int xy3=xy4+tid/1*50;xy3<min(4624, 4624+xy4);xy3+=50*8)
{
for(int f3=f4;f3<min(256, 224+f4);f3+=32)
{
for(int c3=c4;c3<min(128, 128+c4);c3+=128)
{
for(int xy2=xy3;xy2<min(4624, 50+xy3);xy2+=10)
{
for(int f2=f3;f2<min(256, 32+f3);f2+=32)
{
for(int c2=c3;c2<min(128, 128+c3);c2+=16)
{
for(int c1=c2;c1<min(128, 16+c2);c1+=16)
{
for(int xy1=xy2;xy1<min(4624, 10+xy2);xy1+=10)
{
for(int f1=f2;f1<min(256, 32+f2);f1+=32)
{
int ctile=min(16, 128-c1);
int x1=xy1/68;
int y1=xy1%68/1;

int c1_1=c1/1;
int c1_2=c1%1/1;

int kf1_1=f1/16;
int kf1_2=f1%16/1;

int of1_1=f1/1;
int of1_2=f1%1/1;

int offsetA=0+b1*627200+c1_1*4900+1*x1*70+1*y1*1+c1_2*1;
int offsetB=0+kf1_1*18432+c1*144+0*48+0*16+kf1_2*1;
int offsetC=0+b1*1183744+of1_1*4624+x1*68+y1*1+of1_2*1;

if(68-y1>=10){
ukr10x2vCnnb1f256x68y68c128r3s3(A+offsetA, B+offsetB, C+offsetC, ctile, Astrides);
}
else if(68*68-xy1>=10){
for(int sti=68-y1;sti<10;sti+=1)
{
Astrides[sti]+=2;
}

ukr10x2vGemmb1f256x68y68c128r3s3AS(A+offsetA, B+offsetB, C+offsetC, ctile, Astrides);
for(int sti=68-y1;sti<10;sti+=1)
{
Astrides[sti]-=2;
}


}
else{
ukr4x2vGemmb1f256x68y68c128r3s3(A+offsetA, B+offsetB, C+offsetC, ctile, Astrides);
}

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
// end push button generated block
}}